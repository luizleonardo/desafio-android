package org.example.android.concretedribbble;

import android.net.Uri;
import android.text.Html;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luiz on 10/6/2015.
 */
public class GetShotJsonData extends GetRawData {

    private String LOG_TAG = GetShotJsonData.class.getSimpleName();
    private List<Shot> mShots;
    private Uri mDestinationUri;

    public GetShotJsonData(String token){
        super(null);
        createAndUpdateUri(token);
        mShots = new ArrayList<Shot>();
    }

    public void execute(){
        super.setmRawUrl(mDestinationUri.toString());
        DownloadJsonData downloadJsonData = new DownloadJsonData();
        Log.v(LOG_TAG, "Built URL = " + mDestinationUri.toString());
        downloadJsonData.execute(mDestinationUri.toString());
    }

    private boolean createAndUpdateUri(String token) {
        final String DRIBBBLE_API_BASE_URL = "https://api.dribbble.com/v1/shots";
        final String ACCESS_TOKEN_PARAM = "access_token";

        mDestinationUri = Uri.parse(DRIBBBLE_API_BASE_URL).buildUpon()
                .appendQueryParameter(ACCESS_TOKEN_PARAM,token)
                .build();

        return mDestinationUri != null;

    }

    public List<Shot> getShots() {
        return mShots;
    }

    //    public List<Shot> getMShots(){
//        return mShots;
//    }

    public void processResult(){
        if(getmDownloadStatus() != DownloadStatus.OK){
            Log.e(LOG_TAG, "Error downloading the file");
            return;
        }

        final String SHOT_TITLE = "title";
        final String SHOT_ID = "id";
        final String SHOT_DESCRIPTION = "description";
        final String SHOT_VIEWS_COUNT = "views_count";
        final String SHOT_IMG_ITEM = "images";
        final String SHOT_IMG_URL = "normal";
        final String SHOT_USER_ITEM = "user";
        final String SHOT_USER_ID = "id";
        final String SHOT_USER_NAME = "name";
        final String SHOT_USER_AVATAR = "avatar_url";

        try{

            JSONArray jsonShotObjArray = new JSONArray(getmData());

            for(int i = 0; i < jsonShotObjArray.length(); i++) {
                JSONObject jsonShotData = jsonShotObjArray.getJSONObject(i);

                String title = jsonShotData.getString(SHOT_TITLE);
                int id = jsonShotData.getInt(SHOT_ID);

                String description;

                if(jsonShotData.getString(SHOT_DESCRIPTION) != null){
                    description = jsonShotData.getString(SHOT_DESCRIPTION);
                } else {
                    description = "";
                }

               // String description = jsonShotData.getString(SHOT_DESCRIPTION);
                description = stripHtml(description);

                int viewsCount = jsonShotData.getInt(SHOT_VIEWS_COUNT);

                JSONObject jsonShotImage = jsonShotData.getJSONObject(SHOT_IMG_ITEM);
                String image = jsonShotImage.getString(SHOT_IMG_URL);

                JSONObject jsonShotUser = jsonShotData.getJSONObject(SHOT_USER_ITEM);
                int userId = jsonShotUser.getInt(SHOT_USER_ID);
                String userName = jsonShotUser.getString(SHOT_USER_NAME);
                String userAvatar = jsonShotUser.getString(SHOT_USER_AVATAR);

                Shot shotObject = new Shot(id, title, viewsCount, description, new User(userId, userName, userAvatar), image);

                this.mShots.add(shotObject);
            }

            for(Shot singleShot: mShots){
               Log.v(LOG_TAG, singleShot.toString());
            }

        } catch (JSONException jsone) {
            jsone.printStackTrace();
            Log.e(LOG_TAG, "Error processing the json: " + jsone.toString());

        }


    }

    public class DownloadJsonData extends DownloadRawData {
        protected void onPostExecute(String webData) {
            super.onPostExecute(webData);
            processResult();
        }

        protected String doInBackground(String... params){
            String[] par = {mDestinationUri.toString()};
            return super.doInBackground(par);
        }

    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

}
