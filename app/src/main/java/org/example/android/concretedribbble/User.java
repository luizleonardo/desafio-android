package org.example.android.concretedribbble;

import java.io.Serializable;

/**
 * Created by Luiz on 10/6/2015.
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    private int mId;
    private String mName;
    private String mAvatar;

    public User(int mId, String mName, String mAvatar) {
        this.mId = mId;
        this.mName = mName;
        this.mAvatar = mAvatar;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getmId() {
        return mId;
    }

    public String getmName() {
        return mName;
    }

    public String getmAvatar() {
        return mAvatar;
    }

    @Override
    public String toString() {
        return "User{" +
                "mId=" + mId +
                ", mName='" + mName + '\'' +
                ", mAvatar='" + mAvatar + '\'' +
                '}';
    }
}
