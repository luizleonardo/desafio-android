package org.example.android.concretedribbble;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Luiz on 10/7/2015.
 */
public class ShotRecyclerViewAdapter extends RecyclerView.Adapter<ShotImageViewHolder>{
    private List<Shot> mShotsList;
    private Context mContext;


    public ShotRecyclerViewAdapter(Context context, List<Shot> shotsList) {
        mContext = context;
        this.mShotsList = shotsList;
    }

    @Override
    public ShotImageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.browse, null);
        ShotImageViewHolder shotImageViewHolder = new ShotImageViewHolder(view);
        return shotImageViewHolder;
    }

    @Override
    public void onBindViewHolder(ShotImageViewHolder shotImageViewHolder, int i) {
        Shot shotItem = mShotsList.get(i);
        Picasso.with(mContext).load(shotItem.getmImage()).error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(shotImageViewHolder.thumbnail);
        shotImageViewHolder.title.setText(shotItem.getmTitle());

    }

    @Override
    public int getItemCount() {
        return (null != mShotsList ? mShotsList.size() : 0);
    }

    public void loadNewData(List<Shot> newShots){
        mShotsList = newShots;
        notifyDataSetChanged();
    }

    public Shot getShot(int position) {
        return (null != mShotsList ? mShotsList.get(position) : null);
    }

}
