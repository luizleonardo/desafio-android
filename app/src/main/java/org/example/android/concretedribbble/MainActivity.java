package org.example.android.concretedribbble;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String APPLICATION_ACCESS_TOKEN = "ad6b7f6d800eb83b7f143febd3f0abfb32715c7b32bfebe7aadce4211dc474c6";
    private static final String LOG_TAG = "MainActivity";
    private static final String SHOT_TRANSFER = "SHOT_TRANSFER";
    private List<Shot> mShotsList = new ArrayList<Shot>();
    private RecyclerView mRecyclerView;
    private ShotRecyclerViewAdapter shotRecyclerViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        shotRecyclerViewAdapter = new ShotRecyclerViewAdapter(MainActivity.this, new ArrayList<Shot>());
        mRecyclerView.setAdapter(shotRecyclerViewAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this, ViewShotDetailsActivity.class);
                intent.putExtra(SHOT_TRANSFER, shotRecyclerViewAdapter.getShot(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }

        }));

    }

    @Override
    protected void onResume(){
        super.onResume();
        ProcessShots processShots = new ProcessShots(APPLICATION_ACCESS_TOKEN);
        processShots.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class ProcessShots extends GetShotJsonData {

        public ProcessShots(String token) {
            super(token);
        }

        public void execute(){
            super.execute();
            ProcessData processData = new ProcessData();
            processData.execute();
        }

        public class ProcessData extends DownloadJsonData {

            protected void onPostExecute(String webData){
                super.onPostExecute(webData);
                shotRecyclerViewAdapter.loadNewData(getShots());
            }
        }
    }
}
