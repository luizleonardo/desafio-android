package org.example.android.concretedribbble;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ViewShotDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shot_details);

        Intent intent = getIntent();
        Shot shot = (Shot) intent.getSerializableExtra("SHOT_TRANSFER");

        TextView shotTitle = (TextView) findViewById(R.id.shot_title);
        shotTitle.setText(shot.getmTitle());

        TextView shotDescription = (TextView) findViewById(R.id.shot_description);
        shotDescription.setText("Description: " + shot.getmDescription());

        TextView authorDetail = (TextView) findViewById(R.id.shot_author);
        authorDetail.setText("Author: " + shot.getmUser().getmName());

        ImageView authorAvatar = (ImageView) findViewById(R.id.author_avatar);
        Picasso.with(this).load(shot.getmUser().getmAvatar())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(authorAvatar);

        ImageView shotImage = (ImageView) findViewById(R.id.shot_image);
        Picasso.with(this).load(shot.getmImage())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(shotImage);

    }

}
