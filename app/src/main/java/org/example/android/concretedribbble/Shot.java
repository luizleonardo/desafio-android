package org.example.android.concretedribbble;

import java.io.Serializable;

/**
 * Created by Luiz on 10/6/2015.
 */
public class Shot implements Serializable {

    private static final long serialVersionUID = 1L;

    private int mId;
    private String mTitle;
    private int mViewsCount;
    private String mDescription;
    private User mUser;
    private String mImage;

    public Shot(int mId, String mTitle, int mViewsCount, String mDescription, User mUser, String mImage) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mViewsCount = mViewsCount;
        this.mDescription = mDescription;
        this.mUser = mUser;
        this.mImage = mImage;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getmId() {
        return mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public int getmViewsCount() {
        return mViewsCount;
    }

    public String getmDescription() {
        return mDescription;
    }

    public User getmUser() {
        return mUser;
    }

    public String getmImage() {
        return mImage;
    }

    @Override
    public String toString() {
        return "Shot{" +
                "mId=" + mId +
                ", mTitle='" + mTitle + '\'' +
                ", mViewsCount=" + mViewsCount +
                ", mDescription='" + mDescription + '\'' +
                ", mUser=" + mUser.toString() +
                ", mImage='" + mImage + '\'' +
                '}';
    }


}
